package ru.tsc.tsepkov.tm.command;

import ru.tsc.tsepkov.tm.api.model.ICommand;
import ru.tsc.tsepkov.tm.api.service.IServiceLocator;
import ru.tsc.tsepkov.tm.enumerated.Role;

public abstract class AbstractCommand implements ICommand {

    protected IServiceLocator serviceLocator;

    public abstract String getArgument();

    public abstract String getName();

    public abstract String getDescription();

    public abstract void execute();

    public String getUserId() {
        return serviceLocator.getAuthService().getUserId();
    }

    public IServiceLocator getServiceLocator() {
        return serviceLocator;
    }

    public void setServiceLocator(final IServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    public String toString() {
        final String name = getName();
        final String argument = getArgument();
        final String description = getDescription();
        String displayName = "";
        if (name != null && !name.isEmpty()) displayName += name;
        if (argument != null && !argument.isEmpty()) displayName += ", " + argument;
        if (description != null && !description.isEmpty()) displayName += ": " + description;
        return displayName;
    }

    public abstract Role[] getRoles();

}
