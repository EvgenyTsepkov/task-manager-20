package ru.tsc.tsepkov.tm.command.user;

import ru.tsc.tsepkov.tm.api.service.IAuthService;
import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.model.User;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public final class UserRegistryCommand extends AbstractUserCommand {

    public static final String NAME = "user-registry";

    public static final String DESCRIPTION = "Registry user.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[USER REGISTRY]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL:");
        final String email = TerminalUtil.nextLine();
        final IAuthService authService = serviceLocator.getAuthService();
        final User user = authService.registry(login, password, email);
        System.out.println("USER REGISTERED SUCCESSFULLY:");
        System.out.println("LOGIN: " + user.getLogin() + ";");
        System.out.println("ROLE:" + user.getRole() + ";");
        System.out.println("EMAIL: " + user.getEmail() + ".");
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
