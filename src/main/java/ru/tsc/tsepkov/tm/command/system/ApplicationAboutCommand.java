package ru.tsc.tsepkov.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    public static final String ARGUMENT = "-a";

    public static final String NAME = "about";

    public static final String DESCRIPTION = "Show about program.";

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Evgeny Tsepkov");
        System.out.println("e-mail: markmilson@yandex.ru");
    }

}
