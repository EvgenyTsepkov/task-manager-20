package ru.tsc.tsepkov.tm.command.user;

import ru.tsc.tsepkov.tm.enumerated.Role;
import ru.tsc.tsepkov.tm.exception.user.AlreadyLoginException;
import ru.tsc.tsepkov.tm.util.TerminalUtil;

public final class UserLoginCommand extends AbstractUserCommand {

    public static final String NAME = "login";

    public static final String DESCRIPTION = "User login.";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        if (serviceLocator.getAuthService().isAuth()) throw new AlreadyLoginException();
        System.out.println("[USER LOGIN]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD:");
        final String password = TerminalUtil.nextLine();
        serviceLocator.getAuthService().login(login, password);
    }

    @Override
    public Role[] getRoles() {
        return null;
    }

}
