package ru.tsc.tsepkov.tm.exception.user;

public class PermissionException extends AbstractUserException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
