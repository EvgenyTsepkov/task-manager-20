package ru.tsc.tsepkov.tm.exception.field;

public final class ProjectNullException extends AbstractFieldException {

    public ProjectNullException() {
        super("Error! Project is null...");
    }

}
