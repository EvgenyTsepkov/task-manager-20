package ru.tsc.tsepkov.tm.api.repository;

import ru.tsc.tsepkov.tm.model.Task;
import java.util.List;

public interface ITaskRepository extends IUserOwnedRepository<Task> {

    List<Task> findAllByProjectId(String userId, String projectId);

}
