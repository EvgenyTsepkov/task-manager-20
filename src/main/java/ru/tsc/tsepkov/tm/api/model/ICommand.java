package ru.tsc.tsepkov.tm.api.model;

public interface ICommand {

    String getArgument();

    String getName();

    String getDescription();

    void execute();

}
