package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.repository.IUserRepository;
import ru.tsc.tsepkov.tm.model.User;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        for (final User user : records) {
            if (login.equals(user.getLogin())) return user;
        }
        return null;
    }

    @Override
    public User findByEmail(final String email) {
        for (final User user : records) {
            if (email.equals(user.getEmail())) return user;
        }
        return null;
    }

    @Override
    public Boolean isLoginExist(final String login) {
        for (final User user : records) {
            if (login.equals(user.getLogin())) return true;
        }
        return false;
    }

    @Override
    public Boolean isEmailExist(final String email) {
        for (final User user : records) {
            if (email.equals(user.getEmail())) return true;
        }
        return false;
    }

}
