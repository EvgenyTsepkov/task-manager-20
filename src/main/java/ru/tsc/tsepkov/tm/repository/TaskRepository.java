package ru.tsc.tsepkov.tm.repository;

import ru.tsc.tsepkov.tm.api.repository.ITaskRepository;
import ru.tsc.tsepkov.tm.model.Task;
import java.util.ArrayList;
import java.util.List;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    public List<Task> findAllByProjectId(final String userId, final String projectId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : records) {
            if (task.getProjectId() == null) continue;
            if (!userId.equals(task.getUserId())) continue;
            if (task.getProjectId().equals(projectId)) result.add(task);
        }
        return result;
    }

}
